#!/usr/bin/env bash
set -e

# used by setup.py to get OpenBCI version
apt-get -qq -y install git 

# basic Python 3 environment
apt-get -qq -y install python3-pip python3-setuptools