#!/usr/bin/env bash
set -e

apt-get -qq -y install git debhelper sed equivs chrpath python3-all python3-pip

# for some unexplained reason stdeb also need python-dev package installed
apt-get -qq -y install python-dev python3-all-dev


# for building packages
pip3 install stdeb pyqt-distutils

