#!/usr/bin/env bash
set -e

# GPG_KEY - enviroment variable - private key in base64
echo "$GPG_KEY" | base64 -d | gpg --allow-secret-key-import --import -
cd dist

debsigs --sign=origin python3-obci-readmanager*.deb

