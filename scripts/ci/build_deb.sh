#!/usr/bin/env bash
set -e

mkdir dist
python3 setup.py --command-packages=stdeb.command bdist_deb
mv ./deb_dist/*.deb ./dist/
