#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.


import numpy
from obci_readmanager.signal_processing.signal.data_raw_write_proxy import SamplePacket


COUNT = 0

try:
    from .test_auto_ring_buffer import ArrayCallback
except (SystemError, ImportError):
    from test_auto_ring_buffer import ArrayCallback


def test_auto_ring_buffer2():
    from obci_readmanager.signal_processing.buffers import auto_ring_buffer as R

    per, ch = (4, 3)
    callback = ArrayCallback()
    b = R.AutoRingBuffer(5, 5, 5, ch, callback, False)

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[1., 2., 3., 4., 5.],
         [10., 20., 30., 40., 50.],
         [100., 200., 300., 400., 500.]])

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[6., 7., 8., 9., 10.],
         [60., 70., 80., 90., 100.],
         [600., 700., 800., 900., 1000.]])

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[11., 12., 13., 14., 15.],
         [110., 120., 130., 140., 150.],
         [1100., 1200., 1300., 1400., 1500.]])

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[16., 17., 18., 19., 20.],
         [160., 170., 180., 190., 200.],
         [1600., 1700., 1800., 1900., 2000.]])

    b = R.AutoRingBuffer(10, 5, 3, ch, callback, False)

    zero_count()

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[3., 4., 5., 6., 7.],
         [30., 40., 50., 60., 70.],
         [300., 400., 500., 600., 700.]])

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[6., 7., 8., 9., 10.],
         [60., 70., 80., 90., 100.],
         [600., 700., 800., 900., 1000.]])

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[9., 10., 11., 12., 13.],
         [90., 100., 110., 120., 130.],
         [900., 1000., 1100., 1200., 1300.]])

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[12., 13., 14., 15., 16.],
         [120., 130., 140., 150., 160.],
         [1200., 1300., 1400., 1500., 1600.]])
    callback.assert_called(
        [[15., 16., 17., 18., 19.],
         [150., 160., 170., 180., 190.],
         [1500., 1600., 1700., 1800., 1900.]])

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[18., 19., 20., 21., 22.],
         [180., 190., 200., 210., 220.],
         [1800., 1900., 2000., 2100., 2200.]])

    b = R.AutoRingBuffer(10, 10, 2, ch, callback, False)

    zero_count()

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[1., 2., 3., 4., 5., 6., 7., 8., 9., 10.],
         [10., 20., 30., 40., 50., 60., 70., 80., 90., 100.],
         [100., 200., 300., 400., 500., 600., 700., 800., 900., 1000.]])
    callback.assert_called(
        [[3., 4., 5., 6., 7., 8., 9., 10., 11., 12.],
         [30., 40., 50., 60., 70., 80., 90., 100., 110., 120.],
         [300., 400., 500., 600., 700., 800., 900., 1000., 1100., 1200.]])

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[5., 6., 7., 8., 9., 10., 11., 12., 13., 14.],
         [50., 60., 70., 80., 90., 100., 110., 120., 130., 140.],
         [500., 600., 700., 800., 900., 1000., 1100., 1200., 1300., 1400.]])
    callback.assert_called(
        [[7., 8., 9., 10., 11., 12., 13., 14., 15., 16.],
         [70., 80., 90., 100., 110., 120., 130., 140., 150., 160.],
         [700., 800., 900., 1000., 1100., 1200., 1300., 1400., 1500., 1600.]])

    b = R.AutoRingBuffer(10, 3, 8, ch, callback, False)

    zero_count()

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[7., 8., 9.],
         [70., 80., 90.],
         [700., 800., 900.]])

    b.handle_sample_packet(get_sample_packet(per, ch))

    b.handle_sample_packet(get_sample_packet(per, ch))
    callback.assert_called(
        [[15., 16., 17.],
         [150., 160., 170.],
         [1500., 1600., 1700.]])


COUNT = 0


def zero_count():
    global COUNT
    COUNT = 0


def get_sample_packet(per, ch):
    """

    :param per: number of samples
    :param ch: number of channels
    :return: signal as tuple with two numpy arrays
    (numpy.array( [ 1485342990.17, 1485342990.28, 1485342990.42, ...]),
     numpy.array([
                    [ 1, 10, 100, ...],
                    [ 2, 20, 200, ...],
                    [ 3, 30, 300, ...],
                    [ 4, 400, 400, ...]
                ])
    )"""
    global COUNT
    samples = numpy.zeros([per, ch])
    timestamps = numpy.zeros(per)
    for i in range(per):
        COUNT += 1
        for j in range(ch):
            samples[i][j] = 10 ** j * COUNT
        timestamps[i] = 10.0
    return SamplePacket(samples, timestamps)

