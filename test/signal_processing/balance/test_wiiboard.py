# -*- coding: utf-8 -*-
import os

import math

from obci_readmanager.signal_processing.balance.wii_preprocessing import wii_filter_signal, wii_downsample_signal, \
    wii_cut_fragments
from obci_readmanager.signal_processing.balance.wii_analysis import (wii_COP_path, wii_max_sway_AP_MP,
                                                                     wii_mean_COP_sway_AP_ML,
                                                                     wii_RMS_AP_ML, wii_confidence_ellipse_area,
                                                                     wii_mean_velocity,
                                                                     wii_get_percentages_values)
from obci_readmanager.signal_processing.balance.wii_read_manager import WBBReadManager

import matplotlib.pyplot as py


def test_wii_analysis(plot=False):
    """Crude wiiboard test."""
    pth = __file__
    f = {'info': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.xml'),
         'data': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.raw'),
         'tags': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.tag')
         }
    w = WBBReadManager(f['info'], f['data'], f['tags'])
    w.get_x()
    w.get_y()
    wbb_mgr = wii_filter_signal(w, 30.0, 2, use_filtfilt=False)
    wbb_mgr = wii_downsample_signal(wbb_mgr, factor=2, pre_filter=True, use_filtfilt=True)
    smart_tags = wii_cut_fragments(wbb_mgr)
    for sm in smart_tags:
        sm_x = sm.get_channel_samples('x')
        sm_y = sm.get_channel_samples('y')
        py.figure()
        print(wii_COP_path(wbb_mgr, sm_x, sm_y, plot=True))
    if plot:
        py.show()


def test_wbb_tutorial():
    # inicjalizacja klasy WBBReadManager - utwórz obiekt podając na wejściu ścieżki do
    # odpowiednich plików :
    pth = __file__
    f = {'info': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.xml'),
         'data': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.raw'),
         'tags': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.tag')
         }
    wbb_mgr = WBBReadManager(f['info'], f['data'], f['tags'])

    # obiekt klasy WBBReadManager jest podklasą ReadManager
    # umożliwia to pobranie informacji o sygnale:
    float(wbb_mgr.get_param('sampling_frequency'))
    int(wbb_mgr.get_param('number_of_channels'))

    # obiekt klasy  WBBReadManager posiada metody:
    # get_x(): zwraca COPx wyznaczony na podstawie danych z czujników
    wbb_mgr.get_x()

    # get_y(): zwraca COPy wyznaczony na podstawie danych z czujników
    wbb_mgr.get_y()

    # get_timestamps(): zwraca dane z kanału ze znacznikami czasowymi
    wbb_mgr.get_timestamps()

    # filtracja danych przy użyciu funkcji  wii_filter_signal(), która jako parametry przyjmuje:
    # wbb_mgr – obiekt klasy  WBBReadManager
    # cutoff_upper – częstość odcięcia (float)
    # order – rząd filtru (int)
    # use_filtfilt – True/False w zależności czy ma być użyto procedura
    #                          filtrowania filtfilt/lfilter (bool)
    # funkcja zwraca obiekt klasy  WBBReadManager z przefiltrowanym sygnałem
    cutoff_upper = 20
    order = 2
    use_filtfilt = False
    wbb_mgr = wii_filter_signal(wbb_mgr,
                                cutoff_upper,
                                order,
                                use_filtfilt)
    # przepróbkowanie danych przy użyciu funkcji wii_downsample_signal(), która jako parametry
    # przyjmuje:
    # wbb_mgr – obiekt klasy  WBBReadManager
    # factor -  nowa_fs = fs / factor (int)
    # pre_filter - True/False w zależności czy ma być użyty filtr dolnoprzepustowy z częstością
    #                      odcięcia: częstość próbkowania / 2
    # use_filtfilt – True/False w zależności czy ma być użyta procedura
    #                        filtrowania filtfilt/lfilter (bool)
    # funkcja zwraca obiekt klasy  WBBReadManager z przepróbkowanycm sygnałem
    factor = 2
    pre_filter = False
    use_filtfilt = True
    wbb_mgr = wii_downsample_signal(wbb_mgr,
                                    factor,
                                    pre_filter,
                                    use_filtfilt)

    # segmentacja danych przy użyciu funkcji wii_cut_fragments(), która jako parametry
    # przyjmuje:
    # wbb_mgr – obiekt klasy  WBBReadManager
    # start_tag_name – nazwa znacznika określającego początek fragmentu
    # end_tags_names – lista z nazwami znaczników określających koniec fragmentu
    # funkcja zwraca obiekt klasy  SmartTagsManager
    smart_tags = wii_cut_fragments(wbb_mgr,
                                   start_tag_name='start',
                                   end_tags_names=['stop'])
    assert len(smart_tags) == 2


def test_wbb_tutorial_calculations():
    pth = __file__
    f = {'info': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.xml'),
         'data': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.raw'),
         'tags': os.path.join(pth[:-len(os.path.basename(pth))], 'test1.obci.tag')
         }
    wbb_mgr = WBBReadManager(f['info'], f['data'], f['tags'])
    x = wbb_mgr.get_x()
    y = wbb_mgr.get_y()

    # Wyznaczenie wartości maksymalnych wychyleń w kierunku AP i ML przy użyciu
    # funkcji  wii_max_sway_AP_MP(x, y).
    # funkcja przyjmuje jako parametr:
    # x – macierz reprezentująca kanał x
    # y – macierz reprezentująca kanał y
    # funkcja zwraca:
    # max_sway – maksymalne wychwianie (float)
    # max_AP – maksymalne wychwianie w kierunku AP (float),
    # max_ML – maksymalne wychwianie w kierunku ML (float)

    max_sway, max_AP, max_ML = wii_max_sway_AP_MP(x, y)
    assert math.isclose(max_sway, 1.24926602925)
    assert math.isclose(max_AP, 0.963355109482)
    assert math.isclose(max_ML, 0.948074915941)

    # Wyznaczenie średniej wartości wychwiań  COP od punktu (0,0) przy użyciu funkcji
    # wii_mean_COP_sway_AP_ML(x, y).
    # Funkcja przyjmuje jako parametr:
    # x – macierz reprezentująca kanał x,
    # y – macierz reprezentująca kanał y.
    # Funkcja zwraca:
    # cop - średnią wartość wychwiań do punktu (0, 0) (float),
    # mean_x_COP średnią wartość wychwiań do punktu (0, 0) w  ML (float),
    # mean_y_COP = średnią wartość wychwiań do punktu (0, 0) w AP  (float).

    mean_COP, mean_x_COP, mean_y_COP = wii_mean_COP_sway_AP_ML(x, y)
    assert math.isclose(mean_COP, 0.39970248823223942)
    assert math.isclose(mean_x_COP, 0.25619550034987382)
    assert math.isclose(mean_y_COP, 0.25184114671039992)

    # wyznaczenie długość drogi (path length)  COP przy użyciu funkcji
    #  wii_COP_path(wbb_mgr, x, y, plot=False):
    # Funkcja przyjmuje jako parametr:
    # wbb_mgr – obiekt klasy  WBBReadManager
    # x – macierz reprezentująca kanał x
    # y – macierz reprezentująca kanał y
    # plot – True/False (opcjonalnie)
    # Funkcja zwraca:
    # path_length - długość drogi (path length) COP (float),
    # path_length_x - długość drogi (path length) COP w ML (float),
    # path_length_y - - długość drogi (path length) COP w AP (float).

    path_length, path_length_x, path_length_y = wii_COP_path(wbb_mgr, x, y, plot=False)
    assert math.isclose(path_length, 719.6820586485735)
    assert math.isclose(path_length_x, 455.67477599178915)
    assert math.isclose(path_length_y, 461.25732253947632)

    # Wyznaczenie wartości RMS (w AP i ML) przy użyciu funkcji
    #  wii_RMS_AP_ML(x, y).
    # Funkcja przyjmuje jako parametr:
    # x – macierz reprezentująca kanał x,
    # y – macierz reprezentująca kanał y.
    # Funkcja zwraca:
    # RMS – (float),
    # RMS_AP – (float),
    # RMS_ML – (float).

    RMS, RMS_AP, RMS_ML = wii_RMS_AP_ML(x, y)
    assert math.isclose(RMS, 0.45490548534073694)
    assert math.isclose(RMS_AP, 0.32173549078321501)
    assert math.isclose(RMS_ML, 0.32159800149188617)

    # Wyznaczenie 95% powierzchni ufności elipsy przy pomocy
    # funkcji wii_confidence_ellipse_area(x, y).
    # Funkcja przyjmuje jako parametr:
    # x – macierz reprezentująca kanał x,
    # y – macierz reprezentująca kanał y.
    # Funkcja zwraca:
    # e -  95% powierzchnia ufności elipsy (float).

    ellipse = wii_confidence_ellipse_area(x, y)
    assert math.isclose(ellipse, 1.9502853445343999)

    # Wyznaczenie  średniej prędkości przemieszczenia (w AP i ML) przy użyciu funkcji
    #  wii_mean_velocity(wbb_mgr, x, y).
    # Funkcja przyjmuje jako parametr:
    # wbb_mgr – obiekt klasy  WBBReadManager
    # x – macierz reprezentująca kanał x,
    # y – macierz reprezentująca kanał y,
    # plot – True/False (opcjonalnie).
    # Funkcja zwraca:
    # mean_velocity -  średnia prędkość przemieszczenia,
    # velocity_AP  - średnia prędkość przemieszczenia w AP,
    # velocity_ML -  średnia prędkość przemieszczenia w ML.

    mean_velocity, velocity_AP, velocity_ML = wii_mean_velocity(wbb_mgr, x, y)
    assert math.isclose(mean_velocity, 45.839621569972834)
    assert math.isclose(velocity_AP, 29.379447295508047)
    assert math.isclose(velocity_ML, 29.023871082279566)

    # Wyznaczenie  procentowej wartości przebywania w czterech ćwiartkach układu
    # współrzędnych przy użyciu funkcji wii_get_percentages_values(wbb_mgr, x, y, plot).
    # Funkcja przyjmuje jako parametr:
    # wbb_mgr – obiekt klasy  WBBReadManager,
    # x – macierz reprezentująca kanał x,
    # y – macierz reprezentująca kanał y,
    # plot – True/False (opcjonalnie).
    # Funkcja zwraca:
    # top_right – procentowa wartość przebywania w prawej górnej ćwiartce układu współrzędnych # (float),
    # top_left - procentowa wartość przebywania w lewej górnej ćwiartce układu współrzędnych
    # (float),
    # bottom_right - procentowa wartość przebywania w prawej dolnej ćwiartce układu
    # współrzędnych (float),
    # bottom_left -  procentowa wartość przebywania w lewej dolnej ćwiartce układu
    # współrzędnych (float).

    top_right, top_left, bottom_right, bottom_left = wii_get_percentages_values(wbb_mgr, x, y, plot=False)
    assert math.isclose(top_right, 31.528662420382172)
    assert math.isclose(top_left, 25.95541401273886)
    assert math.isclose(bottom_right, 23.487261146496824)
    assert math.isclose(bottom_left, 18.232484076433121)


if __name__ == '__main__':
    test_wii_analysis(plot=True)
