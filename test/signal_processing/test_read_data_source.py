#!/usr/bin/env python3
# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.
import time

import numpy as np
import pytest

from obci_readmanager.signal_processing.signal.data_generic_write_proxy import SamplePacket
from obci_readmanager.signal_processing.signal.data_raw_write_proxy import DataRawWriteProxy
from obci_readmanager.signal_processing.signal.signal_exceptions import NoNextValue


def test_read_data_source(tmpdir):
    # PREPARE SOME SAMPLE FILE *************************************************
    f_name = str(tmpdir.join('./tescik.obci.dat'))
    px = DataRawWriteProxy(f_name)

    def write_single_value(value):
        packet = SamplePacket(ts=np.array([time.time()]), samples=np.array([[value]]))
        px.data_received(packet)

    write_single_value(1.2)
    write_single_value(0.0023)
    write_single_value(-123.456)
    write_single_value(3.3)
    write_single_value(5.0)
    write_single_value(0.0)
    assert px.finish_saving() == (f_name, 6)

    from obci_readmanager.signal_processing.signal import read_data_source as s

    # TEST MEMORY DATA SOURCE **************************************************

    py = s.MemoryDataSource(np.zeros((2, 3)))

    py.set_sample(0, [1.0, 2.0])
    py.set_sample(1, [3.0, 4.0])
    py.set_sample(2, [5.0, 6.0])

    def assert_equal(arr1, arr2):
        assert np.array_equal(np.array(arr1), np.array(arr2))

    assert_equal([i for i in py.iter_samples()],
                 [np.array([1., 2.]), np.array([3., 4.]), np.array([5., 6.])])

    assert_equal([i for i in py.iter_samples()],
                 [np.array([1., 2.]), np.array([3., 4.]), np.array([5., 6.])])

    assert_equal(py.get_samples(0, 1), np.array([[1.], [2.]]))

    with pytest.raises(IndexError):
        py.set_sample(3, [3.0, 4.0])

    with pytest.raises(ValueError):
        py.set_sample(2, [1.0, 2.0, 3.0])

    # TEST FILE DATA SOURCE ****************************************************

    py = s.FileDataSource(f_name, 2)

    assert_equal(py.get_samples(0, 0), np.empty((2, 0), dtype=np.float64))

    assert_equal(np.abs(np.array([[1.20000000e+00, -1.23456000e+02],
                                  [2.30000000e-03, 3.30000000e+00]]) - py.get_samples(0, 2)) < 0.001,
                 [[True, True], [True, True]])

    with pytest.raises(NoNextValue):
        py.get_samples(0, 10)
    assert_equal(np.abs(np.array([[1.20000000e+00, -1.23456000e+02, 5.00000000e+00],
                                  [2.30000000e-03, 3.30000000e+00, 0.00000000e+00]]) - py.get_samples(0, 3)) < 0.001,
                 [[True, True, True],
                  [True, True, True]], )
    with pytest.raises(NoNextValue):
        py.get_samples(1, 3)

    assert_equal(np.abs(np.array([[1.20000000e+00, -1.23456000e+02, 5.00000000e+00],
                                  [2.30000000e-03, 3.30000000e+00, 0.00000000e+00]]) - py.get_samples()) < 0.001,
                 [[True, True, True],
                  [True, True, True]])

    py = s.FileDataSource(f_name, 2)

    from numpy import array

    assert_equal(np.abs(array([[1.20000000e+00, -1.23456000e+02, 5.00000000e+00],
                               [2.30000000e-03, 3.30000000e+00, 0.00000000e+00]]) - py.get_samples()) < 0.001,
                 [[True, True, True],
                  [True, True, True]])

    py = s.FileDataSource(f_name, 2)

    assert_equal(np.abs(array([[1.20000000e+00, -1.23456000e+02, 5.00000000e+00],
                               [2.30000000e-03, 3.30000000e+00, 0.00000000e+00]]) - py.get_samples()) < 0.001,
                 [[True, True, True],
                  [True, True, True]])

    assert_equal(np.abs(array([[1.20000000e+00, -1.23456000e+02, 5.00000000e+00],
                               [2.30000000e-03, 3.30000000e+00, 0.00000000e+00]]) - py.get_samples()) < 0.001,
                 [[True, True, True],
                  [True, True, True]])

    assert_equal([max(abs(i - y)) < 0.0001 for i, y in zip(py.iter_samples(),
                                                           [array([1.2, 0.0023]), array([-123.456, 3.3]),
                                                            array([5., 0.])])],
                 [True, True, True])

    py = s.FileDataSource(f_name, 2)

    assert [max(abs(i - y)) < 0.0001 for i, y in zip(py.iter_samples(),
                                                     [array([1.2, 0.0023]), array([-123.456, 3.3]),
                                                      array([5., 0.])])] == \
           [True, True, True]

    assert [max(abs(i - y)) < 0.0001 for i, y in zip(py.iter_samples(),
                                                     [array([1.2, 0.0023]), array([-123.456, 3.3]),
                                                      array([5., 0.])])] == \
           [True, True, True]
