# -*- coding: utf-8 -*-
# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.

import os

import mne
import numpy
import pytest

from obci_readmanager.signal_processing.read_manager import ReadManager

from obci_readmanager.signal_processing import smart_tags_manager as mgr
from obci_readmanager.signal_processing.tags import smart_tag_definition as df
from test_smart_tags_manager import fabricate_data_file, fabricate_info_file


def test_mne_semicolon():
    pth = os.path.abspath(__file__)

    fname = os.path.join(pth[:-len(os.path.basename(pth))], 'data', 'data.obci')
    read_manager = ReadManager(fname + '.info',
                               fname + '.dat',
                               fname + '_semicolon.tags')
    read_manager.get_mne_raw()


def test_mne_compat():
    """Minimal test which just reads file and checks if mne can do something with it"""
    pth = os.path.abspath(__file__)

    fname = os.path.join(pth[:-len(os.path.basename(pth))], 'data', 'data.obci')
    read_manager = ReadManager(fname + '.info',
                               fname + '.dat',
                               fname + '.tags')
    raw = read_manager.get_mne_raw()
    raw.set_eeg_reference(['M1', 'M2'])
    raw.filter(1, 30)
    # mne has additional "stim" channel
    assert set(read_manager.get_param('channels_names')).issubset(set(raw.ch_names))


def _assert_rms_similar_metadata(rm1, rm2):
    assert rm1.get_param('first_sample_timestamp') == rm2.get_param('first_sample_timestamp')
    assert float(rm1.get_param('sampling_frequency')) == float(rm2.get_param('sampling_frequency'))
    assert rm1.get_param('channels_names') == rm2.get_param('channels_names')
    assert rm1.get_param('number_of_channels') == rm2.get_param('number_of_channels')


def _assert_rms_similar_data(rm1, rm2):
    assert numpy.all(numpy.isclose(rm1.get_microvolt_samples(), rm2.get_microvolt_samples(), rtol=0, atol=1e-10))


def _assert_rms_similar(rm1, rm2):
    _assert_rms_similar_data(rm1, rm2)
    _assert_rms_similar_metadata(rm1, rm2)


def _assert_tags_ok(rm1, rm2):
    tags1 = rm1.get_tags()
    tags2 = rm2.get_tags()
    assert len(tags1) == len(tags2)
    for tag1, tag2 in zip(tags1, tags2):
        assert tag1 == tag2


def test_mne_and_back(tmpdir):
    """Converts data back and forth."""
    pth = os.path.abspath(__file__)

    fname = os.path.join(pth[:-len(os.path.basename(pth))], 'data', 'data.obci')
    read_manager = ReadManager(fname + '.timestamp.info',
                               fname + '.dat',
                               fname + '.tags')
    m = read_manager.get_mne_raw()
    rm2 = ReadManager.from_mne(m)

    _assert_rms_similar(read_manager, rm2)

    # test if converted rm can be written and read
    rm2.save_to_file(str(tmpdir), 'test_rm_mne')
    file3 = os.path.join(str(tmpdir), 'test_rm_mne')
    rm3 = ReadManager(file3 + '.obci.xml',
                      file3 + '.obci.raw',
                      file3 + '.obci.tag', )

    _assert_rms_similar(rm2, rm3)
    _assert_tags_ok(read_manager, rm3)


def test_mne_epochs(tmpdir):
    d = df.SmartTagDurationDefinition(start_tag_name='trigger', start_offset=-0.15,
                                      end_offset=0, duration=1.0)
    pth = __file__
    f = {'info': os.path.join(str(tmpdir), 'nic.obci.svarog.info'),
         'data': os.path.join(str(tmpdir), 'nic.obci.raw'),
         'tags': os.path.join(pth[:-len(os.path.basename(pth))],
                              'data', 'nic.obci.svarog.tags'
                              )
         }

    fabricate_data_file(f['data'])
    fabricate_info_file(f['info'])
    m = mgr.SmartTagsManager(d, f['info'], f['data'], f['tags'])

    def func1(tag):
        try:
            return tag['desc']['value'] == '1'
        except:
            return False

    def func2(tag):
        try:
            return tag['desc']['value'] == '0'
        except:
            return False

    event_descs = ['Positive', 'Negative']

    epochs = m.get_mne_epochs(p_func=[func1, func2], event_descs=event_descs)

    epochs.save(os.path.join(str(tmpdir), 'testmne-epo.fif'))
    epochs = mne.read_epochs(os.path.join(str(tmpdir), 'testmne-epo.fif'))

    assert len(epochs['Positive']) == 26
    assert len(epochs['Negative']) == 25
    assert m.sampling_freq == epochs.info['sfreq']
    assert set(epochs.ch_names) == set(m.get_smart_tags()[0].get_param('channels_names'))

    tags2 = mgr.SmartTagsManager.get_smart_tags_from_mne_epochs(epochs)

    assert len(epochs) == len(tags2)
    _assert_rms_similar_metadata(tags2[0], m.get_smart_tags()[0])

    # assert data is similar (not counting conversional and numerical stability issues)
    # data lengths might differ by 1 sample
    data1 = tags2[0].get_microvolt_samples()
    data2 = m.get_smart_tags()[0].get_microvolt_samples()
    assert numpy.all(numpy.isclose(data1, data2))
