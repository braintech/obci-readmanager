#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.

import os
import glob

import numpy

from obci_readmanager.signal_processing import smart_tags_manager as mgr
from obci_readmanager.signal_processing.tags import smart_tag_definition as df
from obci_readmanager.signal_processing.smart_tags_manager import SmartTagsManager
from obci_readmanager.signal_processing.tags.smart_tag_definition import SmartTagDurationDefinition, \
    SmartTagEndTagDefinition
from obci_readmanager.signal_processing.read_manager import ReadManager


def test_legacy_doctest():
    d = df.SmartTagDurationDefinition(
        start_tag_name='trigger', start_offset=0, end_offset=0, duration=1.0)

    pth = __file__
    f = {'info': os.path.join(pth[:-len(os.path.basename(pth))], 'data', 'nic.obci.svarog.info'),
         'data': 'nic.obci.dat',
         'tags': os.path.join(pth[:-len(os.path.basename(pth))], 'data', 'nic.obci.svarog.tags')
         }

    fabricate_data_file(f['data'])

    m = mgr.SmartTagsManager(d, f['info'], f['data'], f['tags'])
    assert len(m.get_smart_tags()) == 51

    tags, num = iter_all_tags(m)

    assert num == 51
    assert abs(tags[0].get_start_timestamp() - 0.36085605621337891) < 0.0001

    assert abs(tags[50].get_end_timestamp() - 79.996880054473877) < 0.0001

    assert abs(tags[2].get_channel_samples('Fp1')[0] - 19389.0) < 0.0001

    assert tags_len_ok(tags)

    dd = df.SmartTagEndTagDefinition(
        start_tag_name='trigger', start_offset=0, end_offset=0, end_tags_names=['trigger'])

    mm = mgr.SmartTagsManager(dd, f['info'], f['data'], f['tags'])

    tags, num = iter_all_tags(mm)

    assert num == 50

    assert not_duration_tags_data_len(tags) == 20131

    assert not_duration_tags_data_sub(tags)

    # Another test

    from obci_readmanager.signal_processing.tags import tags_file_writer as p

    px = p.TagsFileWriter('./tescik.obci.tags')

    px.tag_received({'start_timestamp': 1.0, 'end_timestamp': 5.0, 'name': 'A', 'channels': '',
                     'desc': {'x': 123, 'y': 456, 'z': 789}})

    px.tag_received({'start_timestamp': 6.0, 'end_timestamp': 10.0, 'name': 'A', 'channels': '',
                     'desc': {'x': 1234, 'y': 4567, 'z': 789}})

    px.tag_received({'start_timestamp': 6.1, 'end_timestamp': 6.5, 'name': 'B', 'channels': '',
                     'desc': {'x': 123, 'y': 456, 'z': 789}})

    px.tag_received({'start_timestamp': 7.0, 'end_timestamp': 7.0, 'name': 'B', 'channels': '',
                     'desc': {'x': 123, 'y': 456, 'z': 789}})

    px.tag_received({'start_timestamp': 7.5, 'end_timestamp': 8.7, 'name': 'B', 'channels': '',
                     'desc': {'x': 123, 'y': 456, 'z': 789}})

    px.tag_received({'start_timestamp': 9.0, 'end_timestamp': 15.0, 'name': 'A', 'channels': '',
                     'desc': {'x': 12345, 'y': 45678, 'z': 789}})

    px.tag_received({'start_timestamp': 10.2, 'end_timestamp': 11.0, 'name': 'B', 'channels': '',
                     'desc': {'x': 123, 'y': 456, 'z': 789}})

    px.tag_received({'start_timestamp': 12.0, 'end_timestamp': 12.5, 'name': 'B', 'channels': '',
                     'desc': {'x': 123, 'y': 456, 'z': 789}})

    px.tag_received({'start_timestamp': 20.0, 'end_timestamp': 25.0, 'name': 'A', 'channels': '',
                     'desc': {'x': 12345, 'y': 45678, 'z': 789}})

    px.finish_saving(0.0)
    './tescik.obci.tags'

    fabricate_data_file('./tescik.obci.dat', 1)

    fabricate_info_file('./tescik.obci.info', 1)

    dd = df.SmartTagEndTagDefinition(
        start_tag_name='A', start_offset=0, end_offset=0, end_tags_names=['A'])

    mm = mgr.SmartTagsManager(dd, './tescik.obci.info',
                              './tescik.obci.dat', './tescik.obci.tags')

    tags = [i for i in mm.iter_smart_tags()]

    assert len(tags) == 3

    tags2 = [i for i in mm]

    assert len(tags2) == 3

    assert tags == tags2

    assert (tags[1].get_all_samples()[0][0]) == 768.0

    dd = df.SmartTagDurationDefinition(
        start_tag_name='B', start_offset=0, end_offset=0, duration=1.0)

    mm = mgr.SmartTagsManager(dd, None, None, None, tags[1])

    tss = [i for i in mm.iter_smart_tags()]

    assert len(tss) == 3

    tss_tags = [t.get_start_tag() for t in tss]

    assert tss_tags == \
           [{'channels': '', 'start_timestamp': 6.0999999999999996, 'desc': {u'y': u'456', u'x': u'123', u'z': u'789'},
             'name': u'B', 'end_timestamp': 6.5}, {'channels': '', 'start_timestamp': 7.0,
                                                   'desc': {u'y': u'456', u'x': u'123', u'z': u'789'}, 'name': u'B',
                                                   'end_timestamp': 7.0},
            {'channels': '', 'start_timestamp': 7.5, 'desc': {u'y': u'456', u'x': u'123', u'z': u'789'},
             'name': u'B', 'end_timestamp': 8.6999999999999993}]

    assert tss[0].get_all_samples()[0][0] == 780.0

    dd = df.SmartTagDurationDefinition(
        start_tag_name='B', start_offset=0, end_offset=0, duration=1.0)

    mm = mgr.SmartTagsManager(dd, None, None, None, tags[2])

    tss = [i for i in mm.iter_smart_tags()]

    assert len(tss) == 2

    tss_tags = [t.get_start_tag() for t in tss]

    assert tss_tags == \
           [{'channels': '', 'start_timestamp': 10.199999999999999, 'desc': {u'y': u'456', u'x': u'123', u'z': u'789'},
             'name': u'B', 'end_timestamp': 11.0}, {'channels': '', 'start_timestamp': 12.0,
                                                    'desc': {u'y': u'456', u'x': u'123', u'z': u'789'}, 'name': u'B',
                                                    'end_timestamp': 12.5}]

    for fl in glob.glob('tescik*'):
        os.remove(fl)

    for fl in glob.glob('nic.obci*'):
        os.remove(fl)


def iter_first_tag(m):
    for st in m.iter_smart_tags():
        print(len(st.get_all_samples()[0]))
        break


def iter_all_tags(m):
    count = 0
    sts = []
    for st in m.iter_smart_tags():
        count = count + 1
        sts.append(st)
    return sts, count


def tags_len_ok(tags):
    for tag in tags:
        for i in range(23):
            if len(tag.get_all_samples()[i]) != 256:
                return False
    return True


def not_duration_tags_data_len(tags):
    l = 0
    for tag in tags:
        print(tag.get_start_timestamp() - tag.get_end_timestamp())
        l += len(tag.get_all_samples()[0])
    return l


def not_duration_tags_data_sub(tags):
    """True if data in tags are sequential -
    they should be, as we have trigger-to-trigger tags
    and data in file are sequential."""
    d = tags[0].get_all_samples()[0][0] - 1
    ret = True
    for tag in tags:
        td_len = len(tag.get_all_samples()[0])
        for j in range(td_len):
            for i in range(23):
                td = tag.get_all_samples()[i]
                if td[j] != d + 1:
                    print(td[j])
                    ret = False
                d = td[j]
    return ret


def fabricate_data_file(f, ch=23):
    import struct
    ch_count = ch
    with open(f, 'wb') as data_file:
        for i in range(100000 * ch_count):
            data_file.write(struct.pack('d', i))


def fabricate_info_file(f, ch=23):
    from obci_readmanager.signal_processing.signal import info_file_proxy
    p = info_file_proxy.InfoFileWriteProxy(f)
    l_signal_params = {}
    l_freq = '128.0'
    l_ch_nums = [str(i) for i in range(ch)]
    l_ch_names = ['O' + str(i) for i in range(ch)]
    l_ch_gains = [str(i) for i in range(ch)]
    l_ch_offsets = [str(i) for i in range(ch)]

    l_signal_params['number_of_channels'] = len(l_ch_nums)
    l_signal_params['sampling_frequency'] = l_freq
    l_signal_params['channels_numbers'] = l_ch_nums
    l_signal_params['channels_names'] = l_ch_names
    l_signal_params['channels_gains'] = l_ch_gains
    l_signal_params['channels_offsets'] = l_ch_offsets
    l_signal_params['number_of_samples'] = 100
    l_signal_params['file'] = 'tescik.obci.dat'
    l_signal_params['first_sample_timestamp'] = 1.0
    p.finish_saving(l_signal_params)


def test_script_from_tutorial():
    """
    Test script from RM tutorial.

    Basic check for crashing.
    """
    # Utwórz definicję cięcia - fragmenty od 100 ms przed Znacznikiem typu "target"
    # do 600 ms po tym Znaczniku
    tag_def = SmartTagDurationDefinition(start_tag_name="trigger", start_offset=-0.1, end_offset=0.0, duration=0.7)

    pth = __file__
    f = {'info': os.path.join(pth[:-len(os.path.basename(pth))], 'data', 'data.obci.info'),
         'data': os.path.join(pth[:-len(os.path.basename(pth))], 'data', 'data.obci.dat'),
         'tags': os.path.join(pth[:-len(os.path.basename(pth))], 'data', 'data.obci.tags')
         }

    # Utwórz obiekt zarządzający
    smgr = SmartTagsManager(tag_def, f['info'], f['data'], f['tags'])

    # alternatywnie:

    mgr = ReadManager(f['info'], f['data'], f['tags'])
    smgr = SmartTagsManager(tag_def, '', '', '', mgr)

    # Uśrednij wszystkie znalezione fragmenty sygnału
    mean = numpy.mean([i.get_samples() for i in smgr], axis=0)
    assert mean.shape == (25, 102)

    # Zsumuj wszystkie fragmenty, których startowy Znacznik spełnia zadane kryteria:
    # Znacznik o długości dwudziestu sekund zaczynający się od piątej sekundy
    # oraz spełniający dodatkowe kryterium func
    tags = smgr.get_smart_tags(p_from=5.0, p_len=20.0, p_func=lambda s_tag: s_tag["desc"]["value"] == "0")
    mean_tags = numpy.mean([i.get_samples() for i in tags], axis=1)
    assert mean_tags.shape == (6, 102)

    # Wypisz wszystkie fragmenty zaczynające się Znacznikiem typu "target",
    # a kończące się pół sekundy przed kolejnym Znacznikiem
    # typu "target" lub "non-target"
    tag_def = SmartTagEndTagDefinition(start_tag_name="trigger", start_offset=-0.1, end_offset=-0.5,
                                       end_tags_names=["trigger", ])
    mgr = SmartTagsManager(tag_def, f['info'], f['data'], f['tags'])
    for i_smart_tag in mgr.iter_smart_tags():
        assert isinstance(i_smart_tag.get_samples(), numpy.ndarray)
